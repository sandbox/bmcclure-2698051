(function ($, Drupal) {

  Drupal.behaviors.offCanvasMenu = {
    attach: function(context, settings) {
      function off_canvas_menu_enabled_menus() {
        var menus = Drupal.settings.offCanvasMenu.menus_enabled;

        var enabled = [];

        $.each(menus, function(index, value) {
          if (value) {
            enabled.push(value);
          }
        });

        return enabled;
      }

      var container = $(Drupal.settings.offCanvasMenu.container_selector);

      if (container.length < 1 || container.hasClass('off-canvas-menu-processed')) {
        return;
      }

      var url = Drupal.settings.basePath + 'off_canvas_menu/ajax/' + off_canvas_menu_enabled_menus().join('+');

      if (url) {
        $.get(url, function (data) {
          $(data).appendTo(container);

          container.addClass('off-canvas-menu-processed');

          $('i.expand', container).click(function () {
            var ul = $(this).parent('a').next('ul');
            if (ul.is(":visible")) {
              ul.slideUp();
              $(this).removeClass('fi-minus').addClass('fi-plus');
            } else {
              $(this).parent('a').parent('ul').find('ul').slideUp();

              ul.slideDown();

              $(this).removeClass('fi-plus').addClass('fi-minus');
            }

            return false;
          });
        });
      }
    }
  };

})(jQuery, Drupal);
