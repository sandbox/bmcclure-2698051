<?php
/**
 * @file
 * Administration page callbacks for the Menu Item View module.
 */

function _off_canvas_menu_get_available_menus() {
  $menus = menu_get_menus();

  $exclude_menus = array();

  if (!empty($exclude_menus)) {
    $exclude_menus = array_combine($exclude_menus, $exclude_menus);
  }

  $available_menus = array_diff_key($menus, $exclude_menus);

  return $available_menus;
}

/**
 * Form builder. Configure Menu Item View
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function off_canvas_menu_admin_settings() {
  $form = array();

  $form['off_canvas_menu_menus_enabled'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled menus'),
    '#description' => t('Enabled menus will be loaded into the off-canvas menu'),
    '#options' => _off_canvas_menu_get_available_menus(),
    '#default_value' => variable_get("off_canvas_menu_menus_enabled", array()),
  );

  $form['off_canvas_menu_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#description' => t('Configure how the off-canvas menu should work'),
    '#collapsible' => false,
    '#collapsed' => false,
  );

  $form['off_canvas_menu_settings']['off_canvas_menu_container_selector'] = array(
    '#type' => 'textfield',
    '#title' => 'Container Selector',
    '#description' => t('The jQuery selector used to target the off-canvas menu container'),
    '#default_value' => variable_get('off_canvas_menu_container_selector', '.left-off-canvas-menu'),
  );

  return system_settings_form($form);
}
